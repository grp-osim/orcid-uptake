# jupyter-notebooks

A collection of OSIM jupyter notebooks for data analysis. 

## Description of each notebook

- **apc_data.ipynb**: Calculates and output basic metrics related to the OSIM APCs data sheet hosted in ownCloud
- **get_unpaywall_data.ipynb**: Retrieve unpaywall data for a collection of articles
- **hybrid_and_oa_journals_in_apc_data.ipynb**: Calculates APCs paid to OA vs hybrid journals in the APC data sheet hosted in ownCloud
- **orcid_uptake.ipynb**: Calculates ORCID uptake across EMBL sites from data held in SAP
- **publications_by_publisher.ipynb**: Parses a report exported from Converis to output the number of publications by journal and by publisher, as well as details of those journals (OA status; EPMC participation)
- **publications_in_epmc.ipynb**: Calculates the proportion of publications in input file that are deposited in Europe PMC
- **publications_in_orcid.ipynb**: Calculates the proportion of publications in input file that are found in ORCID profiles or EMBL researchers
- **query_EPMC_preprints_linkedData.ipynb**: Can be used to query EuropePMC core API to pull preprint and linked data for publications.
- **bioRxiv_API_query_preprint_metadata.ipynb**: Queries bioRxiv API for preprint metadata.

## Installation

This project uses poetry to manage dependencies, so follow the instructions 
at https://python-poetry.org/docs/#installation if poetry is not already 
available in your system.

Then install this project dependencies by running
```
$ poetry install
```

## Configuration

Notebooks in this repository import configuration variables from a configuration file
that is imported under the alias ```conf```.

To create the configuration file, add a ```local/config.py``` file to the root of 
this project's directory. This configuration file should define any of the variables that
are read by the notebooks. For example, if you see the error 
```AttributeError: module 'local.config' has no attribute 'apc_csv'``` when attempting to
execute ```apc_data.ipynb```, you will need to define that variable in your config file:

```
import os

home = os.path.expanduser("~")
apc_csv = os.path.join(home, "Downloads", "apc_data_collection.csv")
```

## Usage

Every notebook in this repository is independent, so you can run any that you are interested in.
If you are new to jupyter notebooks, you might benefit from trying a tutorial first. For example:
- https://www.dataquest.io/blog/jupyter-notebook-tutorial/
